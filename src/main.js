import { createApp } from "vue";
import { createRouter } from "vue-router";
import { createStore } from "vuex";

import {
  CONTEXT_MODULES,
  CONTEXT_COMPONENTS,
  ROUTER_OPTS,
  VUEX_OPTS,
} from "./settings";
import { routes, stores, components } from "./registers";
import App from "./App.vue";

/** برکا */
const app = createApp(App);

/** نصبیدن پودمان‌ها */
for (const req of CONTEXT_MODULES) {
  req.keys().forEach((key) => {
    req(key);
  });
}

/** نصبیدن مولفه‌ها */
for (const req of CONTEXT_COMPONENTS) {
  req.keys().forEach((key) => {
    const component = req(key).default;
    app.component(component.name, component);
  });
}
components.forEach((component) => {
  app.component(component.name, component);
});

/** راهیگر برکا */
const router = createRouter({
  routes: routes,
  ...ROUTER_OPTS,
});

/** انبار برکا */
const store = createStore({
  modules: stores,
  ...VUEX_OPTS,
});

/** شناساندن انبار و راهیگر به ویو و برافراشتن‌اش */
app.use(store).use(router).mount("#app");
