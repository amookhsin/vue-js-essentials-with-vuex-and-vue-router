import { createWebHistory } from "vue-router";

/**
 * پودمانگاه‌ها
 */
export const CONTEXT_MODULES = [
  require.context("@/modules", true, /\.\/[A-Z]\w*$/),
];

/**
 * مولفه‌گاه‌ها
 */
export const CONTEXT_COMPONENTS = [
  require.context("@/components", true, /\.\/[A-Z]\w*\/?\w*\.vue$/),
];

/**
 * پیکریدن ویوروتر
 */
export const ROUTER_OPTS = {
  history: createWebHistory(process.env.BASE_URL),
  base: process.env.BASE_URL,
};

/**
 * پیکریدن ویوایکس
 */
export const VUEX_OPTS = {
  // Vuex Options
};
