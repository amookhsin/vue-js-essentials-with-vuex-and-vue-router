/* jshint esversion: 7 */

const _routes = [];
const _stores = {};
const _components = [];

class Register {
  static routes() {
    return _routes;
  }

  static stores() {
    return _stores;
  }

  static components() {
    return _components;
  }

  static registerRoutes(routes) {
    Register.routes().push(...routes);
  }

  static registerStore(namespace, store) {
    Register.stores()[namespace] = store;
  }

  static registerComponent(component) {
    Register.components().push(component);
  }
}

export const routes = Register.routes();
export const stores = Register.stores();
export const components = Register.components();
export const registerRoutes = Register.registerRoutes;
export const registerStore = Register.registerStore;
export const registerComponent = Register.registerComponent;
