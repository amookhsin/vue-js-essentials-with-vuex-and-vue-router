##########################
#         توسعه          #
##########################
FROM node:14-alpine AS development

# آرگومان‌ها
ARG USERNAME=node
ARG USER_ID=999
ARG USER_GID=${USER_ID}
ARG WORKDIR=/home/${USERNAME}/app

ENV NODE_ENV=development
# برای گذاشتن وابستگی‌های سراسری در پوشه‌ی کاربر ناریشه
ENV NPM_CONFIG_PREFIX=/home/${USERNAME}/.npm-global
# برای افزودن امکان اجرایدن ان‌پی‌ان بدون مشخصیدن مسیرش
ENV PATH=$PATH:/home/${USERNAME}/.npm-global/bin

# نیازمندی‌ها
RUN apk --no-cache add zsh curl git

# ایجادیدن کاربر ناریشه
RUN deluser --remove-home node \
    && addgroup -S ${USERNAME} -g ${USER_GID} \
    && adduser -S -G ${USERNAME} -u ${USER_ID} -D -s /bin/zsh ${USERNAME}
USER ${USERNAME}

# شخصی‌سازی
RUN sh -c "git clone https://github.com/powerline/fonts.git --depth=1 /tmp/fonts && ./tmp/fonts/install.sh" && \
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# پوشه‌کاری
RUN mkdir -p ${WORKDIR}
WORKDIR ${WORKDIR}

# وابستگی‌ها
RUN npm install -g @vue/cli jshint
COPY --chown=${USERNAME}:${USERNAME} ./package.json ./package-lock.json ./
RUN npm install


##########################
#         سازنده         #
##########################
FROM development AS builder

# پوشه‌کاری
RUN mkdir -p /app
WORKDIR /app

# رونشتن پروژه
COPY ./ ./

# ساختن برکا
RUN npm run build


##########################
#          محصول         #
##########################
FROM nginx:alpine AS production

ARG USERNAME=node
ARG USER_ID=999
ARG USER_GID=${USER_ID}
ARG WORKDIR=/home/${USERNAME}/app

ENV NODE_ENV=production
# برای گذاشتن وابستگی‌های سراسری در پوشه‌ی کاربر ناریشه
ENV NPM_CONFIG_PREFIX=/home/${USERNAME}/.npm-global
# برای افزودن امکان اجرایدن ان‌پی‌ان بدون مشخصیدن مسیرش
ENV PATH=$PATH:/home/${USERNAME}/.npm-global/bin

# ایجادیدن کاربر ناریشه
RUN deluser --remove-home node \
    && addgroup -S ${USERNAME} -g ${USER_GID} \
    && adduser -S -G ${USERNAME} -u ${USER_ID} ${USERNAME}
USER ${USERNAME}

# نیازمندی‌ها

# رونشتن پروژه
COPY --chown=${USERNAME}:${USERNAME} ./ ./

# درگاه
EXPOSE 8080

# دستور آغارین
CMD [ "npm", "run", "serve -- --port 8080" ]
